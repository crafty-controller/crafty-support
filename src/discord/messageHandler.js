import { EmbedBuilder, PermissionsBitField } from 'discord.js';
import config from '../../config/config.js';

import Logger from '../lib/logger.js';
const logger = new Logger('CLEANUP');

export async function handleMessage (message) {
	const { guild, author, member, channelId } = message;

	try {
		// Cleanup Exempt if Admin, Staff or bot
		if (
			member.permissions.has(PermissionsBitField.Flags.Administrator) ||
			config.moderatorRoles.some(id => member.roles.cache.has(id)) ||
			member.user.bot
		) {	return;	}

		// Delete message if posted in support channel
		if (config.supportChannels.some(chanObj => chanObj.channelId === channelId)) {
			await message.delete();

			// DM the user to tell them to use a support channel
			const embed = new EmbedBuilder()
				.setAuthor({ name: `${guild.name} Support`, iconURL: await guild.iconURL() })
				.setColor('#E74C3C')
				.setDescription(
					`To recieve support in <#${channelId}> please use \`/support\` `,
					'any other messages to this channel will be discarded.',
				);

			await author.send({ embeds: [ embed ] });
		}
	} catch (err) {
		logger.error('Error while handling message delete:', err);
	}
}
