import { Client, GatewayIntentBits, Partials } from 'discord.js';

let bot;

/**
 * Singleton method that will always return the same client, once instantiated
 * @return {Discord.Client}
 */
export function getInstance () {
	if (bot !== undefined) return bot;

	bot = new Client({
		partials: [
			Partials.Message,
			Partials.Reaction,
			Partials.User,
			Partials.Channel,
		],
		intents: [
			GatewayIntentBits.Guilds, // Thread creation deletion, etc
			GatewayIntentBits.GuildIntegrations, // Deployment of (/) cmds
			GatewayIntentBits.GuildMessages, // Message Management
			GatewayIntentBits.GuildMessageReactions, // Reaction Management
			GatewayIntentBits.DirectMessages, // Bot DM Management
			GatewayIntentBits.DirectMessageReactions, // Bot DM Reactions
		],
	});
	return bot;
}
